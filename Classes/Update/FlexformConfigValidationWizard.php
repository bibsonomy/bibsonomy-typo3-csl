<?php

namespace AcademicPuma\ExtBibsonomyCsl\Update;

use AcademicPuma\ExtBibsonomyCsl\Utils\WizardUtils;
use Doctrine\DBAL\DBALException;
use RuntimeException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\ReferenceIndexUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\RepeatableInterface;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class FlexformConfigValidationWizard implements UpgradeWizardInterface, RepeatableInterface
{
    /**
     * Identifier for this upgrade wizard
     */
    const IDENTIFIER = "extBibsonomyCsl_flexformConfigValidationWizard";

    /**
     * Namespace for the registry
     */
    const REGISTRY_NAMESPACE = "FlexformConfigValidationWizard";

    /**
     * Table to migrate citation stylesheets from
     */
    const STYLESHEET_TABLE = "tx_extbibsonomycsl_domain_model_citationstylesheet";

    /**
     * Table to migrate records from
     */
    const TT_CONTENT = "tt_content";

    /**
     * Table column holding the migration to be
     */
    const FLEXFORM_COL = "pi_flexform";

    /**
     * @var ResourceStorage
     */
    protected $storage;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return self::IDENTIFIER;
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Validate flexform configurations for PUMA/BibSonomy TYPO3 plugins in the TYPO3 database.';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'This upgrade wizards validates the flexform configurations for every PUMA/BibSonomy TYPO3 plugin instance in the "tt_content" database table. If the entry fails the validation, it will be fixed accordingly. When new fields have been added, the wizard will set the current default values for the field.';
    }

    /**
     * Execute the update
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     */
    public function executeUpdate(): bool
    {
        // Check, if PHP extension "Simple XML" is enabled
        if (!extension_loaded('simplexml')) {
            // echo "Simple XML is required for this upgrade wizard. Please check your PHP configuration and enable the extension.";
            return false;
        }

        try {
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            $queryBuilder = $connectionPool->getQueryBuilderForTable(self::TT_CONTENT);
            $queryBuilder->getRestrictions()
                ->removeAll()
                ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

            $limit = 5000;
            $offset = 0;

            do {
                $queryBuilder
                    ->select('uid', 'pid', 'cruser_id', WizardUtils::LIST_TYPE, self::FLEXFORM_COL)
                    ->from(self::TT_CONTENT)
                    ->where(
                        $queryBuilder->expr()->eq(WizardUtils::LIST_TYPE, $queryBuilder->createNamedParameter(WizardUtils::PUBLICATION_LIST_TYPE))
                    )
                    ->orWhere(
                        $queryBuilder->expr()->eq(WizardUtils::LIST_TYPE, $queryBuilder->createNamedParameter(WizardUtils::TAG_CLOUD_TYPE))
                    )
                    ->orderBy('uid')
                    ->setMaxResults($limit)
                    ->setFirstResult($offset);
                $records = $queryBuilder->execute()->fetchAll();
                foreach ($records as $record) {
                    $this->validateEntry($record);
                }
                $offset += $limit;
            } while (count($records) > 0);
        } catch (\Exception $e) {
            // Silently catch db errors
            // echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Validate a single entry.
     *
     * @param array $entry
     */
    protected function validateEntry(array $entry): void
    {
        // sanity checks
        if (empty($entry[self::FLEXFORM_COL])) {
            return;
        }

        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);

        $original = simplexml_load_string($entry[self::FLEXFORM_COL]);
        $validator = new FlexformValidator();
        $validated = $validator->validate($original, $entry[WizardUtils::LIST_TYPE]);

        // Validate selected citation stylesheet
        if ($entry[WizardUtils::LIST_TYPE] == WizardUtils::PUBLICATION_LIST_TYPE) {
            $stylesheetMode = WizardUtils::getXMLFieldValue($validated, "settings.layout.stylesheet");
            if ($stylesheetMode == "default") {
                // Validate, if stylesheet mode is default
                $legacyStylesheetId = WizardUtils::getXMLFieldValue($original, "settings.bib_stylesheet");
                $legacyStylesheet = $legacyStylesheetId ? $this->getStylesheet($legacyStylesheetId, $entry['cruser_id']) : [];
                $validated = $validator->validateStylesheet($validated, $legacyStylesheetId, $legacyStylesheet);
            }
        }

        if ($validated) {
            $queryBuilder = $connectionPool->getQueryBuilderForTable(self::TT_CONTENT);
            $queryBuilder->update(self::TT_CONTENT)
                ->set(self::FLEXFORM_COL, $validated->asXML())
                ->where(
                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($entry['uid'], \PDO::PARAM_INT))
                )
                ->execute();
        }
    }

    protected function getStylesheet(string $id, string $cruserId): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable(self::STYLESHEET_TABLE);
        $queryBuilder
            ->select('uid', 'pid', 'cruser_id', 'id')
            ->from(self::STYLESHEET_TABLE)
            ->where(
                $queryBuilder->expr()->eq('id', $queryBuilder->createNamedParameter($id)),
                $queryBuilder->expr()->eq('cruser_id', $queryBuilder->createNamedParameter($cruserId, \PDO::PARAM_INT))
            )
            ->orderBy('uid');
        $result = $queryBuilder->execute()->fetchAll();

        if ($result) {
            return $result[0];
        }

        return [];
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool Whether an update is required (TRUE) or not (FALSE)
     */
    public function updateNecessary(): bool
    {
        return true;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [
            // Ensures that the database table fields are up-to-date.
            DatabaseUpdatedPrerequisite::class,
            // The reference index needs to be up-to-date.
            ReferenceIndexUpdatedPrerequisite::class,
        ];
    }
}