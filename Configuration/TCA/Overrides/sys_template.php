<?php
defined('TYPO3') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('ext_bibsonomy_csl', 'Configuration/TypoScript', 'BibSonomy CSL');
