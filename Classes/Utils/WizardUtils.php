<?php

namespace AcademicPuma\ExtBibsonomyCsl\Utils;

class WizardUtils
{
    /**
     * Table column containing the list type
     */
    const LIST_TYPE = "list_type";

    /**
     * List type for publication list plugin instances
     */
    const PUBLICATION_LIST_TYPE = "extbibsonomycsl_publicationlist";

    /**
     * List type for tag cloud plugin instances
     */
    const TAG_CLOUD_TYPE = "extbibsonomycsl_tagcloud";

    public const PUBLICATIONLIST_LEGACY_SHEETS = [
        "bib_general" => "s_content",
        "bib_ordering" => "s_sorting",
        "bib_layout" => "s_layout",
        "bib_login" => "s_authentication",
    ];

    public const TAGCLOUD_LEGACY_SHEETS = [
        "bib_general" => "s_content",
        "bib_login" => "s_authentication",
    ];

    public const PUBLICATIONLIST_FIELDS = [
        // Content selection
        "settings.content.sourceType" => ["settings.bib_content_key", "settings.content.sourceType"],
        "settings.content.sourceId" => ["settings.bib_content_value", "settings.content.sourceId"],
        "settings.content.search" => ["settings.bib_search", "settings.content.search"],
        "settings.content.tagsSelect" => ["settings.bib_tags", "settings.content.tagsSelect"],
        "settings.content.tagsUnselect" => ["settings.bib_unselect_tags", "settings.content.tagsUnselect"],
        "settings.content.conceptsSelect" => ["settings.bib_concepts", "settings.content.conceptsSelect"],
        "settings.content.conceptsUnselect" => ["settings.bib_unselect_concepts", "settings.content.conceptsUnselect"],
        "settings.content.maxCount" => ["settings.bib_maxcount", "settings.content.maxCount"],
        "settings.content.searchType" => ["settings.content.searchType"],

        //  Grouping, sorting, filtering
        "settings.grouping.key" => ["settings.bib_grouping", "settings.grouping.key"],
        "settings.grouping.anchors" => ["settings.bib_display_anchors", "settings.grouping.anchors"],
        "settings.sorting.sortKey" => ["settings.bib_sorting", "settings.sorting.sortKey"],
        "settings.sorting.sortOrder" => ["settings.bib_sorting_order", "settings.sorting.sortOrder"],
        "settings.filtering.entrytypes" => ["settings.bib_entrytypes", "settings.filtering.entrytypes"],
        "settings.filtering.numericYear" => ["settings.non_numeric_year", "settings.filtering.numericYear"],
        "settings.filtering.year" => ["settings.bib_year_filter", "settings.filtering.year"],
        "settings.filtering.duplicates" => ["settings.bib_filter_duplicates", "settings.filtering.duplicates"],

        // Layout
        "settings.layout.language" => ["settings.bib_language", "settings.layout.language"],
        "settings.layout.stylesheet" => ["settings.layout.stylesheet"],
        "settings.layout.stylesheetDefault" => ["settings.bib_stylesheet", "settings.layout.stylesheetDefault"],
        "settings.layout.stylesheetName" => ["settings.layout.stylesheetName"],
        "settings.layout.stylesheetXML" => ["settings.bib_ownstylesheet", "settings.layout.stylesheetXML"],
        "settings.layout.thumbnail" => ["settings.layout.thumbnail"],
        "settings.layout.linkDocument" => ["settings.bib_link_doc", "settings.layout.linkDocument"],
        "settings.layout.linkAbstract" => ["settings.bib_link_abstract", "settings.layout.linkAbstract"],
        "settings.layout.linkBibtex" => ["settings.bib_link_bibtex", "settings.layout.linkBibtex"],
        "settings.layout.linkCsl" => ["settings.bib_link_csl", "settings.layout.linkCsl"],
        "settings.layout.linkEndnote" => ["settings.bib_link_endnote", "settings.layout.linkEndnote"],
        "settings.layout.linkUrl" => ["settings.bib_link_url", "settings.layout.linkUrl"],
        "settings.layout.linkDoi" => ["settings.bib_link_doi", "settings.layout.linkDoi"],
        "settings.layout.linkHost" => ["settings.bib_link_host", "settings.layout.linkHost"],
        "settings.layout.linkMisc" => ["settings.bib_link_misc", "settings.layout.linkMisc"],
        "settings.layout.notes" => ["settings.bib_show_notes", "settings.layout.notes"],
        "settings.layout.inlineFilter" => ["settings.bib_inline_filter", "settings.layout.inlineFilter"],
        "settings.layout.curlyBraces" => ["settings.curly_braces", "settings.layout.curlyBraces"],
        "settings.layout.treatBackslashes" => ["settings.treat_backslashes", "settings.layout.treatBackslashes"],
        "settings.layout.bibtexCleaning" => ["settings.bibtex_cleaning", "settings.layout.bibtexCleaning"],
        "settings.layout.bibtexEscape" => ["settings.bibtex_escape_special", "settings.layout.bibtexEscape"],
        "settings.layout.bibtexHtmlEncode" => ["settings.bibtex_htmlencode_special", "settings.layout.bibtexHtmlEncode"],

        // Custom
        "settings.custom.authorsMode" => ["settings.bib_link_personal_mode", "settings.custom.authorsMode"],
        "settings.custom.authorsList" => ["settings.bib_link_personal", "settings.custom.authorsList"],
        "settings.custom.css" => ["settings.bib_css_attribute_for_list_item", "settings.custom.css"],

        // Authentication
        "settings.auth.type" => ["settings.authconfig", "settings.auth.type"],
        "settings.auth.custom.host" => ["settings.bib_server", "settings.auth.custom.host"],
        "settings.auth.custom.userName" => ["settings.bib_login_name", "settings.auth.custom.userName"],
        "settings.auth.custom.apiKey" => ["settings.bib_api_key", "settings.auth.custom.apiKey"],
        "settings.auth.beauth.user" => ["settings.authentication", "settings.auth.beauth.user"],
        "settings.auth.ssl" => ["settings.ssl_verify", "settings.auth.ssl"],
        "settings.auth.sslPath" => ["settings.sslPath", "settings.auth.sslPath"],

        // About
        "settings.debug" => ["settings.debug"],
    ];

    public const TAGCLOUD_FIELDS = [
        // Content selection
        "settings.tags.sourceType" => ["settings.bib_tag_content_key", "settings.tags.sourceType"],
        "settings.tags.sourceId" => ["settings.bib_tag_content_value", "settings.tags.sourceId"],
        "settings.tags.related" => ["settings.bib_tags", "settings.tags.related"],
        "settings.tags.blacklist" => ["settings.bib_blacklist_tags", "settings.tags.blacklist"],
        "settings.tags.maxcount" => ["settings.bib_tag_maxcount", "settings.tags.maxcount"],

        // Authentication
        "settings.auth.type" => ["settings.authconfig", "settings.auth.type"],
        "settings.auth.custom.host" => ["settings.bib_server", "settings.auth.custom.host"],
        "settings.auth.custom.userName" => ["settings.bib_login_name", "settings.auth.custom.userName"],
        "settings.auth.custom.apiKey" => ["settings.bib_api_key", "settings.auth.custom.apiKey"],
        "settings.auth.beauth.user" => ["settings.authentication", "settings.auth.beauth.user"],
        "settings.auth.ssl" => ["settings.ssl_verify", "settings.auth.ssl"],
        "settings.auth.sslPath" => ["settings.sslPath", "settings.auth.sslPath"],

        // About
        "settings.debug" => ["settings.debug"],
    ];

    public static function getXMLField(\SimpleXMLElement $element, string $fieldName): ?\SimpleXMLElement
    {
        return self::getFirstElementByXPath($element, self::getXPathForField($fieldName));
    }

    public static function getXMLFieldValue(\SimpleXMLElement $element, string $fieldName): ?\SimpleXMLElement
    {
        return self::getFirstElementByXPath($element, self::getXPathForFieldValue($fieldName));
    }

    public static function getFirstElementByXPath(\SimpleXMLElement $element, string $path): ?\SimpleXMLElement
    {
        $field = $element->xpath($path);
        if ($field) {
            return $field[0];
        }

        return null;
    }

    private static function getXPathForField(string $fieldName): string
    {
        return "//field[@index='$fieldName']";
    }

    private static function getXPathForFieldValue(string $fieldName): string
    {
        return "//field[@index='$fieldName']/value";
    }

    public static function getXPathForSheet(string $sheetName): string
    {
        return "//sheet[@index='$sheetName']";
    }

}