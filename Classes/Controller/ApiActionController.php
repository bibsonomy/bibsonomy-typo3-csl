<?php

/**
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 * (c) 2022 Kevin Choong <choong.kvn@gmail.com>
 *          Sebastian Böttger <boettger@cs.uni-kassel.de>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository;
use AcademicPuma\RestClient\Authentication\BasicAuthAccessor;
use AcademicPuma\RestClient\Authentication\OAuthAccessor;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * ApiActionController
 */
class ApiActionController extends ActionController
{
    protected $host;
    protected $accessor;

    /**
     * authenticationRepository
     *
     * @var AuthenticationRepository
     */
    protected $authenticationRepository = null;

    /**
     * @param AuthenticationRepository $authenticationRepository
     */
    public function injectAuthenticationRepository(AuthenticationRepository $authenticationRepository)
    {
        $this->authenticationRepository = $authenticationRepository;
    }

    public function makeAccessor(): void
    {
        $proxy = $GLOBALS['TYPO3_CONF_VARS']['HTTP']['proxy'] ?: "";
        $authSettings = $this->settings['auth'];

        if ($authSettings['type'] == "beauth") {
            $auth = $this->authenticationRepository->findByUid($authSettings['beauth']['user']);

            if (empty($auth)) {
                $message = LocalizationUtility::translate("LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:module.api.authentication.error",
                    'ExtBibsonomyCsl');
                $this->addFlashMessage($message, "", AbstractMessage::ERROR);
                return;
            }

            $host = $auth->getHostAddress();
            if (!$auth->isEnableOAuth()) {
                $apiUser = $auth->getHostUserName();
                $apiKey = $auth->getHostApiKey();
                $this->accessor = new BasicAuthAccessor($host, $apiUser, $apiKey, $proxy);
            } else {
                $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('ext_bibsonomy_csl');
                $this->accessor = new OAuthAccessor($host, unserialize($auth->getSerializedAccessToken(), $proxy),
                    $extensionConfiguration['oauthConsumerToken'], $extensionConfiguration['oauthConsumerSecret']);
            }
        } else {
            $host = $authSettings['custom']['host'];
            $apiUser = $authSettings['custom']['userName'];
            $apiKey = $authSettings['custom']['apiKey'];
            $this->accessor = new BasicAuthAccessor($host, $apiUser, $apiKey, $proxy);
        }

        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host): void
    {
        $this->host = $host;
    }

}