<?php
defined('TYPO3') || die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'ExtBibsonomyCsl',
    'PublicationList',
    'Publication List',
    'ext-bibsonomy-csl-icon'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'ExtBibsonomyCsl',
    'TagCloud',
    'Tag Cloud',
    'ext-bibsonomy-csl-icon'
);

// plugin signature: <extension key without underscores> '_' <plugin name in lowercase>
$publicationListPluginSignature = 'extbibsonomycsl_publicationlist';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$publicationListPluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $publicationListPluginSignature,
    // Flexform configuration schema file
    'FILE:EXT:ext_bibsonomy_csl/Configuration/FlexForms/flexform_publicationlist.xml'
);

$tagCloudPluginSignature = 'extbibsonomycsl_tagcloud';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$tagCloudPluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $tagCloudPluginSignature,
    // Flexform configuration schema file
    'FILE:EXT:ext_bibsonomy_csl/Configuration/FlexForms/flexform_tagcloud.xml'
);