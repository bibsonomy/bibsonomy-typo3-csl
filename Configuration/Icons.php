<?php

return [
    'ext_bibsonomy_csl-plugin-publicationlist' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:ext_bibsonomy_csl/Resources/Public/Icons/user_plugin_publicationlist.svg'
    ],
    'ext_bibsonomy_csl-plugin-tagcloud' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:ext_bibsonomy_csl/Resources/Public/Icons/user_plugin_tagcloud.svg'
    ],
];
