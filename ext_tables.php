<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'ExtBibsonomyCsl',
        'system',
        'backend',
        '',
        [
            \AcademicPuma\ExtBibsonomyCsl\Controller\BackendController::class => 'index',
            \AcademicPuma\ExtBibsonomyCsl\Controller\CitationStylesheetController::class => 'list, new, create, edit, update, delete, upload, import',
            \AcademicPuma\ExtBibsonomyCsl\Controller\AuthenticationController::class => 'list, new, create, edit, update, delete, oauth',
        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:ext_bibsonomy_csl/Resources/Public/Icons/Extension.svg',
            'labels' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf',
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_extbibsonomycsl_domain_model_citationstylesheet', 'EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_csh_tx_extbibsonomycsl_domain_model_citationstylesheet.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_extbibsonomycsl_domain_model_citationstylesheet');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_extbibsonomycsl_domain_model_authentication', 'EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_csh_tx_extbibsonomycsl_domain_model_authentication.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_extbibsonomycsl_domain_model_authentication');
})();
