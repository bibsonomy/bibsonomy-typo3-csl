<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'PUMA/BibSonomy CSL',
    'description' => 'TYPO3 extension for PUMA and BibSonomy using CSL',
    'category' => 'plugin',
    'author' => 'Kevin Choong, Sebastian Böttger',
    'author_email' => 'choong.kvn@gmail.com, boettger@cs.uni-kassel.de',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '3.3.6',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
