<?php

/**
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 * (c) 2022 Kevin Choong <choong.kvn@gmail.com>
 *          Sebastian Böttger <boettger@cs.uni-kassel.de>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository;
use AcademicPuma\ExtBibsonomyCsl\Utils\ApiUtils;
use AcademicPuma\ExtBibsonomyCsl\Utils\PostUtils;
use AcademicPuma\RestClient\Config\Grouping;
use AcademicPuma\RestClient\Config\ModelUtils;
use AcademicPuma\RestClient\Config\Resourcetype;
use AcademicPuma\RestClient\Config\Sorting;
use AcademicPuma\RestClient\Model\Post;
use AcademicPuma\RestClient\Model\Posts;
use AcademicPuma\RestClient\RESTClient;
use Exception;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Seboettg\CiteProc\Exception\CiteProcException;
use Seboettg\CiteProc\StyleSheet;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * PublicationController
 */
class PublicationController extends ApiActionController
{

    const POST_COUNT_LIMIT = 250;

    /**
     * citationStylesheetRepository
     *
     * @var CitationStylesheetRepository
     */
    protected $citationStylesheetRepository = null;

    /**
     * @param CitationStylesheetRepository $citationStylesheetRepository
     */
    public function injectCitationStylesheetRepository(CitationStylesheetRepository $citationStylesheetRepository)
    {
        $this->citationStylesheetRepository = $citationStylesheetRepository;
    }

    /**
     * action list
     *
     * @return string
     * @throws CiteProcException
     */
    public function listAction(): string
    {
        // create API accessor
        $this->makeAccessor();

        // assign settings to view
        $this->view->assignMultiple([
            'settings' => $this->settings,
            'grouping' => $this->settings['grouping'],
            'sorting' => $this->settings['sorting'],
            'filtering' => $this->settings['filtering'],
            'layout' => $this->settings['layout'],
            'custom' => $this->settings['custom'],
        ]);

        // assign citation stylesheet
        switch ($this->settings['layout']['stylesheet']) {
            case 'default':
                $databaseId = $this->settings['layout']['stylesheetDefault'];
                if (!is_numeric($databaseId)) {
                    // ID is not numeric, it means it's a default CSL of the plugin we have to load
                    $fileContent = file_get_contents(GeneralUtility::getFileAbsFileName("EXT:ext_bibsonomy_csl/Resources/Private/CSL/$databaseId"));
                    if ($fileContent) {
                        $this->view->assign('stylesheet', $fileContent);
                    } else {
                        $this->view->assign('stylesheet', StyleSheet::loadStyleSheet('apa'));
                    }

                } else {
                    $stylesheet = $this->citationStylesheetRepository->findByUid($databaseId);
                    $this->view->assign('stylesheet', $stylesheet->getXmlSource());
                }
                break;
            case 'xml':
                $this->view->assign('stylesheet', $this->settings['layout']['stylesheetXML']);
                break;
            case 'name':
            default:
                try {
                    $this->view->assign('stylesheet', StyleSheet::loadStyleSheet($this->settings['layout']['stylesheetName']));
                } catch (CiteProcException $e) {
                    $this->view->assign('stylesheet', StyleSheet::loadStyleSheet('apa'));
                }
                break;
        }

        // get posts
        $posts = $this->getPosts($this->settings);
        $rawPosts = $this->getPosts($this->settings, true);

        // generate raw post map, list hash and set keywords for BibTeX in each post
        $rawPostsMap = [];
        $titles = [];
        foreach ($rawPosts as $post) {
            $rawPostsMap[$post->getResource()->getIntraHash()] = $post;
            $titles[] = $post->getResource()->getTitle();
            $post->getResource()->setKeywords($post->getTag()->toArray());
        }

        // temporary fix for when post is not in raw post map
        foreach ($posts as $post) {
            if (!array_key_exists($post->getResource()->getIntraHash(), $rawPostsMap)) {
                $rawPostsMap[$post->getResource()->getIntraHash()] = $post;
                $titles[] = $post->getResource()->getTitle();
                $post->getResource()->setKeywords($post->getTag()->toArray());
            }
        }

        $this->view->assign('listHash', md5(implode('', $titles)));

        // filtering, grouping & sorting of posts
        $this->filterPosts($posts, $this->settings['filtering']);
        $this->groupAndSortPosts($posts, $this->settings);

        // prepare entrytype list with labels
        if ($this->settings['grouping']['key'] == 'entrytype') {
            $entrytypeLabels = $this->getEntrytypeLabels(array_keys($posts->toArray()), $this->settings['layout']['language']);
            $this->view->assign('entrytypes', $entrytypeLabels);
        }

        // assign posts
        $this->view->assign('posts', $posts);
        $this->view->assign('rawPosts', $rawPostsMap);

        // create author links
        $authorsMode = $this->settings['custom']['authorsMode'];
        $authorsList = $this->settings['custom']['authorsList'];
        if ($authorsMode != 'none' && $authorsList) {
            $entries = explode(PHP_EOL, $authorsList);
            $authorLinks = [];
            foreach ($entries as $entry) {
                $authorLink = explode(';', $entry);
                if (count($authorLink) == 3) {
                    $authorLinks[$authorLink[0]] = [
                        'name' => $authorLink[0],
                        'url' => $authorLink[1] != 'none' ? $authorLink[1] : '',
                        'email' => $authorLink[2] != 'none' ? $authorLink[2] : '',
                    ];
                }
            }
            $this->view->assign('authorLinks', $authorLinks);
        }

        return $this->view->render();
    }

    /**
     * action show
     *
     * @param string $intraHash
     * @param string $userName
     * @return string
     */
    public function showAction(string $intraHash, string $userName): string
    {
        // create API accessor
        $this->makeAccessor();

        // create REST client
        $client = ApiUtils::getRestClient($this->accessor, $this->settings);

        try {
            $client->getPostDetails($userName, $intraHash);
            $post = $client->model();
        } catch (BadResponseException|GuzzleException|Exception $e) {
            $post = new Post();
        }

        $this->view->assign('post', $post);

        return $this->view->render();
    }

    public function getPosts(array $settings, bool $raw = false): Posts
    {
        // create REST client
        $client = ApiUtils::getRestClient($this->accessor, $settings);

        // get settings for request and model
        $content = $settings['content'];
        $grouping = $content['sourceType'];
        $groupingName = $content['sourceId'];
        $tags = $this->getFilterTags($client, $content);
        $search = $content['search'];
        $maxCount = $content['maxCount'];
        $searchType = $content['searchType'] === 'local' ? 'local' : 'searchindex';
        $sortKeys = [];
        $sortOrders = [];

        // handle search index supported operations, like sorted results and filtering by fields
        if ($searchType === 'searchindex') {
            // handle sorted results
            $sorting = $settings['sorting'];
            $sortKeys[] = $this->mapSortKey($sorting['sortKey']);
            $sortOrders[] = $sorting['sortOrder'];

            // handle filtering by year
            $filterYear = $settings['filtering']['year'];
            if ($filterYear) {
                $filterYearSearch = "year:\"{$filterYear}\"";
                $search = $search ? $search . " AND {$filterYearSearch}" : $filterYearSearch;
            }
        }

        $resourceType = $grouping === Grouping::PERSON ? Resourcetype::GOLD_STANDARD_PUBLICATION : Resourcetype::BIBTEX;

        $layout = $settings['layout'];
        $treatCurlyBraces = intval($layout['curlyBraces']);
        $treatBackslashes = intval($layout['treatBackslashes']);
        $bibtexCleaning = $layout['bibtexCleaning'] === '1';

        // retrieve posts as raw in terms of their BibTeX body
        if ($raw) {
            $treatCurlyBraces = ModelUtils::CB_KEEP;
            $treatBackslashes = ModelUtils::BS_KEEP;
            $bibtexCleaning = false;
        }

        // get posts
        try {
            $result = [];
            $limit = !empty($maxCount) ? intval(filter_var($maxCount, FILTER_SANITIZE_NUMBER_INT)) : 100;

            if ($limit > self::POST_COUNT_LIMIT) {
                for ($i = 1; ($i * self::POST_COUNT_LIMIT) <= $limit + self::POST_COUNT_LIMIT; ++$i) {
                    $start = ($i - 1) * self::POST_COUNT_LIMIT;
                    $end = $start + self::POST_COUNT_LIMIT;
                    if ($end > $limit) {
                        $end = $limit;
                    }

                    $subResult = $client
                        ->getPosts($resourceType, $grouping, $groupingName, $tags, '', $search, $sortKeys, $sortOrders, $searchType, $start, $end)
                        ->model($treatCurlyBraces, $treatBackslashes, $bibtexCleaning)
                        ->toArray();
                    $result = array_merge($result, $subResult);
                }
            } else {
                $result = $client
                    ->getPosts($resourceType, $grouping, $groupingName, $tags, '', $search, $sortKeys, $sortOrders, $searchType, 0, $limit)
                    ->model($treatCurlyBraces, $treatBackslashes, $bibtexCleaning)
                    ->toArray();
            }
        } catch (BadResponseException|GuzzleException|Exception $e) {
            return new Posts();
        }

        return new Posts($result);
    }

    private function filterPosts(Posts &$posts, array $settings): void
    {
        $filterDuplicates = $settings['duplicates'];
        $filterYear = $settings['year'];
        $filterNumericYear = $settings['numericYear'];
        $filterEntrytypes = $settings['entrytypes'] != "" ? explode(" ", $settings['entrytypes']) : [];

        $hashes = array();

        foreach ($posts as $key => $post) {
            $resource = $post->getResource();
            $postYear = $resource->getYear();
            $postEntrytype = $resource->getEntrytype();

            // Filter posts with not the filter year
            if ($filterYear && $postYear !== $filterYear) {
                unset($posts[$key]);
                continue;
            }

            // Filter posts with non-numeric year
            if (!$filterNumericYear && !is_numeric($postYear)) {
                unset($posts[$key]);
                continue;
            }

            // Filter posts with not in the included entrytypes
            if ($filterEntrytypes && !in_array($postEntrytype, $filterEntrytypes)) {
                unset($posts[$key]);
                continue;
            }

            // Filter duplicates
            if ($filterDuplicates > 0) {
                // Either use inter- or intrahash depending on the plugin settings
                $hash = $filterDuplicates == 1 ? $post->getResource()->getIntraHash() : $post->getResource()->getInterHash();

                if (in_array($hash, $hashes)) {
                    unset($posts[$key]);
                } else {
                    $hashes[] = $hash;
                }
            }
        }
    }

    private function groupAndSortPosts(Posts &$posts, array $settings)
    {
        $grouping = $settings['grouping'];
        $groupingKey = $grouping['key'];

        $sorting = $settings['sorting'];
        $sortKey = $sorting['sortKey'];
        $sortOrder = $sorting['sortOrder'];

        // grouping & sorting
        if ($groupingKey != 'none') {
            $posts = $this->prepareGrouping($posts, $groupingKey);

            // Set the sorting within the group for DBLP grouping
            if ($grouping == 'dblp') {
                $sortKey = 'dblp';
                $sortOrder = 'desc';
            }

            // sort within sublists of groups
            $groupKeys = array_keys($posts->toArray()); //get groups
            foreach ($groupKeys as $groupKey) {
                if ($posts[$groupKey] instanceof Posts) {
                    $sublist = new Posts($posts[$groupKey]->toArray());
                } else {
                    $sublist = new Posts($posts[$groupKey]);
                }
                // Only sort, when local is used, because results are not already sorted. Also sort for dblp, because of special handling
                if ($sortKey === 'local' || $sortKey === 'dblp') {
                    $sublist->sort($sortKey, $sortOrder === 'desc' ? Sorting::ORDER_DESC : Sorting::ORDER_ASC);
                }
                $posts[$groupKey] = $sublist;
            }
        } else {
            // Only sort, when local is used, because results are not already sorted. Also sort for dblp, because of special handling
            if ($sortKey === 'local' || $sortKey === 'dblp') {
                $posts->sort($sortKey, $sortOrder === 'desc' ? Sorting::ORDER_DESC : Sorting::ORDER_ASC);
            }
            $p = clone $posts;
            $posts->clear();
            $posts->add('', $p);
        }
    }

    /**
     * Prepares type order and filters publications (grouping by type) or does
     * pre-sorting (grouping by year). This function also transforms <code>$posts</code>
     * to an grouped Posts ArrayList.
     * for instance:
     * <pre>
     * array(
     *   [2010] => array(
     *          [0] => pub1
     *          [1] => pub2)
     *   [2014] => array(
     *          [0] => pub3
     *          [1] => pub5)
     * )
     * </pre>
     *
     * @param Posts $posts un-grouped Posts ArrayList
     * @param string $grouping
     * @return Posts
     */
    private function prepareGrouping(Posts &$posts, string $grouping): Posts
    {
        switch ($grouping) {
            case 'entrytype':
                $posts->sort('entrytype', Sorting::ORDER_ASC);
                $this->transformToEntrytypeGroupedArray($posts, $this->settings['sorting']);
                break;
            case 'yearAsc':
                //pre-sorting (asc) for year grouping
                $posts->sort('year', Sorting::ORDER_ASC);
                PostUtils::transformToYearGroupedArray($posts);
                break;
            case 'dblp':
            case 'yearDesc':
                //pre-sorting (desc) for year grouping
            default:
                $posts->sort('year', Sorting::ORDER_DESC);
                PostUtils::transformToYearGroupedArray($posts);
        }

        return $posts;
    }

    private function transformToEntrytypeGroupedArray(Posts &$posts, array $settings)
    {
        $sortKey = $settings['sortKey'];
        $sortOrder = $settings['sortOrder'];

        $processed = [];
        for ($i = 0; $i < count($posts); ++$i) {
            $entrytype = $posts[$i]->getResource()->getEntrytype();
            $processed[$entrytype][] = $posts[$i];
        }

        $posts->replace([]);
        foreach (array_keys($processed) as $group) {
            $subList = new Posts($processed[$group]);
            $subList->sort($sortKey, $sortOrder);
            $posts->add($group, $subList);
        }
    }

    private function getEntrytypeLabels(array $entrytypes, string $lang = 'en-US'): array
    {
        $result = [];
        if ($this->settings['auth']['host'] !== null && str_contains($this->settings['auth']['host'], 'wueresearch')) {
            $customEntrytypesCsv = file_get_contents(
                GeneralUtility::getFileAbsFileName('EXT:ext_bibsonomy_csl/Resources/Private/Custom/wueresearch/entrytypes.csv')
            );

            if ($customEntrytypesCsv) {
                $customEntrytypesArr = explode(PHP_EOL, $customEntrytypesCsv);
                if ($customEntrytypesArr) {
                    foreach ($customEntrytypesArr as $entrytype) {
                        $entryArr = explode(',', $entrytype);
                        if (count($entryArr) == 4) {
                            $label = $lang == 'de-DE' ? $entryArr[2] : $entryArr[1];
                            $result[$entryArr[0]] = [
                                'label' => $label,
                                'description' => $label,
                            ];
                        }
                    }
                }
            }
        }

        foreach ($entrytypes as $entrytype) {
            if (!array_key_exists($entrytype, $result)) {
                $label = LocalizationUtility::translate("entrytype.$entrytype", 'ExtBibsonomyCsl');
                $label = $label ?: $entrytype;
                $description = LocalizationUtility::translate("entrytype.$entrytype.desc", 'ExtBibsonomyCsl');
                $description = $description ?: '';
                $result[$entrytype] = [
                    'label' => $label,
                    'description' => $description,
                ];
            }
        }

        return $result;
    }

    private function getFilterTags(RESTClient $client, array $settings): array
    {
        $tags = [];

        $groupingName = $settings['sourceId'];
        $tagsSelected = $settings['tagsSelect'];
        $tagsUnselected = $settings['tagsUnselect'];
        $conceptsSelected = $settings['conceptsSelect'];
        $conceptsUnselected = $settings['conceptsUnselect'];

        // Add tags
        if (!empty($tagsSelected)) {
            $tags = array_merge($tags, explode(" ", $tagsSelected));
        }

        // Exclude content with tags by prepending sys:not: to the tags
        if (!empty($tagsUnselected)) {
            $tagsUnselectedArr = explode(" ", trim($tagsUnselected));
            foreach ($tagsUnselectedArr as $tagUn) {
                $tags[] = 'sys:not:' . trim(filter_var($tagUn, FILTER_SANITIZE_URL));
            }
        }

        // Add subtags from concept
        if (!empty($conceptsSelected)) {
            $concepts = explode(" ", $conceptsSelected);

            foreach ($concepts as $concept) {
                try {
                    $query = $client->getConceptDetails($concept, $groupingName);
                    foreach ($query->model()->getSubTags() as $subtag) {
                        $tags[] = $subtag->getName();
                    }
                } catch (BadResponseException|GuzzleException|Exception $e) {

                }
            }
        }

        // Exclude content with concepts by prepending sys:not: to the subtags
        if (!empty($conceptsUnselected)) {
            $concepts = explode(" ", $conceptsUnselected);

            foreach ($concepts as $concept) {

                try {
                    $query = $client->getConceptDetails($concept, $groupingName);
                    foreach ($query->model()->getSubTags() as $subtag) {
                        $tags[] = 'sys:not:' . trim(filter_var($subtag, FILTER_SANITIZE_URL));
                    }
                } catch (BadResponseException|GuzzleException|Exception $e) {

                }
            }
        }

        return $tags;
    }

    private function mapSortKey(string $sortKey): string
    {
        switch ($sortKey) {
            case 'author':
                return 'author';
            case 'title':
                return 'title';
            case 'entrytype':
                return 'entrytype';
            case 'month':
                return 'month';
            case 'year':
            default:
                return 'pubdate';
        }
    }
}
