<?php

/**
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 * (c) 2022 Kevin Choong <choong.kvn@gmail.com>
 *          Sebastian Böttger <boettger@cs.uni-kassel.de>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\ExtBibsonomyCsl\Utils\BackendUtils;
use AcademicPuma\ExtBibsonomyCsl\Utils\CitationUtils;
use AcademicPuma\RestClient\Renderer\CSLModelRenderer;
use Closure;
use Exception;
use Seboettg\CiteProc\CiteProc;
use Seboettg\CiteProc\Exception\CiteProcException;
use Seboettg\CiteProc\StyleSheet;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * CitationViewHelper
 */
class CitationViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this->registerArgument('post', '\AcademicPuma\RestClient\Model\Post', 'The post to render as citation', true);
        $this->registerArgument('language', 'string', 'The language', true);
        $this->registerArgument('stylesheet', 'string', 'The CSL stylesheet as XML', true);
        $this->registerArgument('checkStylesheet', 'string', 'Validate the CSL stylesheet before attempt to render citation', false);
    }

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ): string
    {
        $post = $arguments['post'];
        $language = $arguments['language'] ?: 'en-US';
        $stylesheet = $arguments['stylesheet'] ?: StyleSheet::loadStyleSheet('apa');
        $checkStylesheet = $arguments['checkStylesheet'] == '1';

        if ($checkStylesheet && !BackendUtils::isStylesheetValid($stylesheet)) {
            return '-';
        }

        $authorFunction = function($cslItem, $renderedText) {
            return CitationUtils::authorWithoutLink($cslItem, $renderedText);
        };

        $citationNumberFunction = function($cslItem, $renderedText) {
            return CitationUtils::hidden($cslItem, $renderedText);
        };

        $titleFunction = function($cslItem, $renderedText) {
            return CitationUtils::titleWithoutLink($cslItem, $renderedText);
        };

        $additionalMarkup = [
            "bibliography" => [
                "author" => [
                    'function' => $authorFunction,
                    'affixes' => true
                ],
                "citation-number" => [
                    'function' => $citationNumberFunction,
                    'affixes' => true
                ],
                "title" => [
                    'function' => $titleFunction,
                    'affixes' => true
                ]
            ]
        ];

        $output = '';
        try {
            $cslRenderer = new CSLModelRenderer();
            $citeProc = new CiteProc($stylesheet, $language, $additionalMarkup);
            $csl = $cslRenderer->render($post);
            $output .= $citeProc->render(json_decode(json_encode(array($csl)))); // TODO refactor
        } catch (Exception | CiteProcException $e) {
            return $e->getMessage();
        }
        return $output;
    }
}