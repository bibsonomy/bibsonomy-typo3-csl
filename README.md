BibSonomy/PUMA CSL
=============

[![TYPO3 10](https://img.shields.io/badge/TYPO3-10-orange.svg)](https://get.typo3.org/version/10)
[![Latest Stable Version](http://poser.pugx.org/academicpuma/ext-bibsonomy-csl/v)](https://extensions.typo3.org/extension/ext_bibsonomy_csl) 
[![Latest Unstable Version](http://poser.pugx.org/academicpuma/ext-bibsonomy-csl/v/unstable)](https://bitbucket.org/bibsonomy/bibsonomy-typo3-csl/)
[![Total Downloads](http://poser.pugx.org/academicpuma/ext-bibsonomy-csl/downloads)](https://packagist.org/packages/academicpuma/ext-bibsonomy-csl)
[![License](http://poser.pugx.org/academicpuma/ext-bibsonomy-csl/license)](https://packagist.org/packages/academicpuma/ext-bibsonomy-csl) 
[![PHP Version Require](http://poser.pugx.org/academicpuma/ext-bibsonomy-csl/require/php)](https://www.php.net/releases/7_2_0.php)

[BibSonomy/PUMA CSL](https://extensions.typo3.org/extension/ext_bibsonomy_csl) is a TYPO3 extension to include publication lists from the social bookmarking and publication sharing systems [BibSonomy](https://www.bibsonomy.org), (academic) [PUMA](https://www.academic-puma.de) or from other sources.

Features
--------
* Display publication lists from BibSonomy/PUMA by using the [BibSonomy REST API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API)
* Display publications from different sources in various styles (CSL Standard)
* Display your BibSonomy/PUMA tag cloud
* Layout bibliographies or publication lists with Citation Style Language (CSL) templates. See [citationstyles.org](https://citationstyles.org/) for more information or existing citation stylesheets.
* Add and manage your own citation styles within TYPO3.

Usage
-----
Have a look at our [documentation](https://docs.typo3.org/typo3cms/extensions/ext_bibsonomy_csl/1.1.1/Manual/Index.html#introduction).

Development
-----------
This extension uses the Extbase framework and the Fluid template engine.

Before starting development of ext_bibsonomy_csl it is recommended to study the TYPO3 documentation.

Change log
----------

### Version 3.3.6 ###
* Add searchtype selection to switch between 'Search Index' and 'Local' query (Default: Search Index)
* Apply certain query params, when search index is selected
* Update RestClient dependency

### Version 3.3.5 ###
* Fixed limit issues for the FlexformUpdateWizard
* Fixed bool flags just being copied and not properly converted in the FlexformUpdateWizard
* Fixed PHP8 compatibility issues
* Supporting only TYPO3 v11.5+ and PHP 8.0+ from now on

### Version 3.3.3 ###
* Supporting search index operations for certain host systems
* Supporting TYPO3 proxy settings for HTTP requests
* Fixed empty citation showing, when the citation style has citation numbers

### Version 3.3.2 ###
* Added citation stylesheet debug information, when the option has been activated
* Fixed spacing and formatting break around the title in each citation

### Version 3.3.1 ###

* Fixed old default stylesheet selection not being properly copied when using the update wizard
* Fixed old parameters being copied but incorrect due to inconsistency of the values

### Version 3.3.0 ###

* Categorize flexform configuration for better maintenance
* Update wizard for current and future changes to the flexform

### Version 3.2.3 ###

* Fixed previous flexform settings not loading for tagcloud
* Fixed various escape/encoding options for LaTeX and BibTeX not working
* Fixed citation not rendering all fields

### Version 3.2.2 ###

* Fixed previous flexform settings not loading

### Version 3.2.1 ###

* Fixed thumbnails and documents broken
* Fixed entrytype labels
* Fixed tag cloud not working
* Fixed incorrect user IDs set
* Fixed treatment options ignored
* Added fallback for locales and CSL stylesheet

### Version 3.2.0 ###

* Rewritten the plugin and not rely on jquery
* More plain JavaScript and no initializing buttons after load
* Update to latest citeproc to support better affix handling
* New default citation stylesheets
* Updated backend module layout
* Entrytypes are now sorted by the BibTeX entrytype and not the converted CSL types anymore
* Updated plugin flexform layout

### Version 3.1.1 ###

* Fixing JavaScript issues with certain browsers

### Version 3.1.0 ###

* Support for TYPO3 v10+
* Implemented new PHP citeproc
* Optional citations numbers
* Display plugin version in TYPO3 backend
* Various bug fixes

### Version 3.0.0 ###

* Support for TYPO3 v9+
* Support for route enhancers in version 9
* Added instructions on how to use route enhancers for this plugin and removing cHash in URLsq
