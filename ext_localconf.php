<?php
defined('TYPO3') || die();

(static function() {
    if (TYPO3_MODE === 'BE') {
        $icons = [
            'ext-bibsonomy-csl-icon' => 'Extension.svg',
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($icons as $identifier => $path) {
            if (!$iconRegistry->isRegistered($identifier)) {
                $iconRegistry->registerIcon(
                    $identifier,
                    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                    ['source' => 'EXT:ext_bibsonomy_csl/Resources/Public/Icons/' . $path]
                );
            }
        }
    }

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'ExtBibsonomyCsl',
        'PublicationList',
        [
            \AcademicPuma\ExtBibsonomyCsl\Controller\PublicationController::class => 'list, show',
            \AcademicPuma\ExtBibsonomyCsl\Controller\DocumentController::class => 'show, download'
        ],
        // non-cacheable actions
        []
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'ExtBibsonomyCsl',
        'TagCloud',
        [
            \AcademicPuma\ExtBibsonomyCsl\Controller\TagController::class => 'list'
        ],
        // non-cacheable actions
        []
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    publicationlist {
                        iconIdentifier = ext-bibsonomy-csl-icon
                        title = LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_be.xlf:extbibsonomycsl.publicationlist.name
                        description = LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_be.xlf:extbibsonomycsl.publicationlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = extbibsonomycsl_publicationlist
                        }
                    }
                    tagcloud {
                        iconIdentifier = ext-bibsonomy-csl-icon
                        title = LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_be.xlf:extbibsonomycsl.tagcloud.name
                        description = LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_be.xlf:extbibsonomycsl.tagcloud.description
                        tt_content_defValues {
                            CType = list
                            list_type = extbibsonomycsl_tagcloud
                        }
                    }
                }
                show = *
            }
       }'
    );

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['extBibsonomyCsl_flexformConfigValidationWizard']
        = \AcademicPuma\ExtBibsonomyCsl\Update\FlexformConfigValidationWizard::class;
})();
