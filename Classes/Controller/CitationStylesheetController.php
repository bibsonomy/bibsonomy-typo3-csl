<?php

/**
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 * (c) 2022 Kevin Choong <choong.kvn@gmail.com>
 *          Sebastian Böttger <boettger@cs.uni-kassel.de>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet;
use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository;
use AcademicPuma\RestClient\Renderer\XMLModelUnserializer;
use Exception;
use Seboettg\CiteProc\StyleSheet;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\File\ExtendedFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * CitationStylesheetController
 */
class CitationStylesheetController extends ActionController
{

    /**
     * citationStylesheetRepository
     *
     * @var CitationStylesheetRepository
     */
    protected $citationStylesheetRepository = null;

    /**
     * @param CitationStylesheetRepository $citationStylesheetRepository
     */
    public function injectCitationStylesheetRepository(CitationStylesheetRepository $citationStylesheetRepository)
    {
        $this->citationStylesheetRepository = $citationStylesheetRepository;
    }

    /**
     * action list
     *
     * @return string
     */
    public function listAction(): string
    {
        global $GLOBALS;
        $userId = $GLOBALS["BE_USER"]->user["uid"];

        $citationStylesheets = $this->citationStylesheetRepository->findByCruserId($userId);
        $this->view->assign('citationStylesheets', $citationStylesheets);

        // load example post for example citation
        $exampleXml = file_get_contents(GeneralUtility::getFileAbsFileName("EXT:ext_bibsonomy_csl/Resources/Private/CSL/example.xml"));
        $modelUnserializer = new XMLModelUnserializer($exampleXml);
        $post = $modelUnserializer->convertToModel();
        $this->view->assign('post', $post);

        return $this->view->render();
    }

    /**
     * action new
     *
     * @return string
     */
    public function newAction(): string
    {
        return $this->view->render();
    }

    /**
     * action upload
     *
     * @return string
     */
    public function uploadAction(): string
    {
        return $this->view->render();
    }

    /**
     * action import
     *
     * @return string
     */
    public function importAction(): string
    {
        return $this->view->render();
    }

    /**
     * action create
     *
     * @param CitationStylesheet $newCitationStylesheet
     * @throws StopActionException
     * @throws IllegalObjectTypeException
     */
    public function createAction(CitationStylesheet $newCitationStylesheet)
    {
        global $GLOBALS;
        $userId = $GLOBALS["BE_USER"]->user["uid"];
        $newCitationStylesheet->setCruserId($userId);

        if ($this->request->hasArgument('file')) {
            $file = $this->request->getArgument('file');

            if ($file) {
                $fileUtility = GeneralUtility::makeInstance(ExtendedFileUtility::class);
                $fileName = $fileUtility->getUniqueName($file['name'],
                    GeneralUtility::getFileAbsFileName('uploads/'), 1);
                GeneralUtility::upload_copy_move($file['tmp_name'], $fileName);

                $uploadXml = file_get_contents($fileName);
                $newCitationStylesheet->setXmlSource($uploadXml);
            } else {
                $error = LocalizationUtility::translate("LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:module.citationstylesheet.import.error",
                    'ExtBibsonomyCsl');
                $this->addFlashMessage($error, 'ERROR', AbstractMessage::ERROR);
                $this->redirect('list');
                return;
            }
        }

        if ($this->request->hasArgument('import')) {
            $import = $this->request->getArgument('import');
            try {
                $importXml = StyleSheet::loadStyleSheet($import);
                $newCitationStylesheet->setXmlSource($importXml);
            } catch (Exception $e) {
                $error = LocalizationUtility::translate("LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:module.citationstylesheet.import.error",
                    'ExtBibsonomyCsl');
                $this->addFlashMessage($error, 'ERROR', AbstractMessage::ERROR);
                $this->redirect('list');
                return;
            }
        }

        $message = LocalizationUtility::translate("LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:module.citationstylesheet.new.success",
            'ExtBibsonomyCsl');
        $this->addFlashMessage($message, "", AbstractMessage::OK);
        $this->citationStylesheetRepository->add($newCitationStylesheet);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param CitationStylesheet $citationStylesheet
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("citationStylesheet")
     * @return string
     */
    public function editAction(CitationStylesheet $citationStylesheet): string
    {
        $this->view->assign('citationStylesheet', $citationStylesheet);

        return $this->view->render();
    }

    /**
     * action update
     *
     * @param CitationStylesheet $citationStylesheet
     * @throws IllegalObjectTypeException
     * @throws StopActionException
     * @throws UnknownObjectException
     */
    public function updateAction(CitationStylesheet $citationStylesheet)
    {
        $message = LocalizationUtility::translate("LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:module.citationstylesheet.edit.success",
            'ExtBibsonomyCsl');
        $this->addFlashMessage($message, "", AbstractMessage::OK);
        $this->citationStylesheetRepository->update($citationStylesheet);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param CitationStylesheet $citationStylesheet
     * @throws IllegalObjectTypeException
     * @throws StopActionException
     */
    public function deleteAction(CitationStylesheet $citationStylesheet)
    {
        $message = LocalizationUtility::translate("LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:module.citationstylesheet.delete.success",
            'ExtBibsonomyCsl');
        $this->addFlashMessage($message, "", AbstractMessage::WARNING);
        $this->citationStylesheetRepository->remove($citationStylesheet);
        $this->redirect('list');
    }
}
