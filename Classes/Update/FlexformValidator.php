<?php

namespace AcademicPuma\ExtBibsonomyCsl\Update;

use AcademicPuma\ExtBibsonomyCsl\Utils\WizardUtils;
use AcademicPuma\RestClient\Util\StringUtils;
use SimpleXMLElement;

class FlexformValidator
{

    /**
     * Default citation stylesheet name to fall back to
     */
    const DEFAULT_STYLESHEET = "everyAware.csl";

    /**
     * List of all preset citation stylesheet of the plugin
     */
    const PRESET_STYLESHEETS = [
        "apa.csl",
        "chicago-library-list.csl",
        "din-1505-2.csl",
        "everyAware.csl",
        "harvard-limerick.csl",
        "ieee.csl",
        "mhra.csl",
        "mla.csl",
        "nature.csl",
        "lncs.csl",
    ];

    /**
     * Validate a single entry.
     *
     * @param SimpleXMLElement $entry
     * @param string $listType
     * @return SimpleXMLElement|null
     */
    public function validate(SimpleXMLElement $entry, string $listType): ?SimpleXMLElement
    {
        switch ($listType) {
            case WizardUtils::PUBLICATION_LIST_TYPE:
                return  $this->validateForPublicationList($entry);
            case WizardUtils::TAG_CLOUD_TYPE:
                return $this->validateForTagCloud($entry);
            default:
                return null;
        }
    }

    /**
     * Validate the selected CSL stylesheet. Set the value correctly for legacy stylesheet selection
     *
     * @param SimpleXMLElement $entry
     * @param string|null $legacyStylesheetId
     * @param array|null $legacyStylesheet
     * @return SimpleXMLElement|null
     */
    public function validateStylesheet(SimpleXMLElement $entry, ?string $legacyStylesheetId, ?array $legacyStylesheet): SimpleXMLElement
    {
        if ($legacyStylesheetId != null) {
            $selectedStylesheet = WizardUtils::getXMLField($entry, "settings.layout.stylesheetDefault");
            if ($legacyStylesheet) {
                $selectedStylesheet->value = $legacyStylesheet["uid"];
            }

            // all legacy preset stylesheets are urls, hence we extract the file name to use for the new ID for preset stylesheets
            $fileName = $legacyStylesheetId;

            if (strrpos($fileName, "/")) {
                $fileName = substr($fileName, strrpos($fileName, "/") + 1);
            }

            if (!StringUtils::endsWith($fileName, ".csl")) {
                $fileName .= ".csl";
            }

            $selectedStylesheet->value = in_array($fileName, self::PRESET_STYLESHEETS) ? $fileName : self::DEFAULT_STYLESHEET;
        }

        return $entry;
    }

    public function validateForPublicationList(SimpleXMLElement $toValidate): SimpleXMLElement
    {
        // Priorities legacy sheets, so we get rid of duplicate sheets with different names (due to bug in 3.2.X versions)
        $this->handleLegacySheets($toValidate, WizardUtils::PUBLICATIONLIST_LEGACY_SHEETS);

        $config = simplexml_load_file(__DIR__ . "/../../Resources/Private/Update/PublicationListFlexformDefault.xml");
        $this->applyValidatedConfig($toValidate, $config, WizardUtils::PUBLICATIONLIST_FIELDS);

        // Validate language code
        $langCode = WizardUtils::getXMLField($config, "settings.layout.language");
        if ($langCode && !str_contains($langCode->value, "-")) {
            $langCode->value = "en-US";
        }

        // Validate grouping key
        $groupingKey = WizardUtils::getXMLField($config, "settings.grouping.key");
        if ($groupingKey) {
            switch ($groupingKey->value) {
                case "type":
                    $groupingKey->value = "entrytype";
                    break;
                case "year_asc":
                    $groupingKey->value = "yearAsc";
                    break;
                case "year_desc":
                    $groupingKey->value = "yearDesc";
                    break;
                default:
                    break;
            }
        }

        // Validate personal author links mode
        $authorsMode = WizardUtils::getXMLField($config, "settings.custom.authorsMode");
        if ($authorsMode->value == "modal") {
            $authorsMode->value = "link";
        }

        // Validate thumbnail and document link
        $thumbnail = WizardUtils::getXMLField($config, "settings.layout.thumbnail");
        $linkDocument = WizardUtils::getXMLField($config, "settings.layout.linkDocument");
        if (!is_numeric($linkDocument->value)) {
            switch ($linkDocument->value) {
                case "nothing":
                    $thumbnail->value = "0";
                    $linkDocument->value = "0";
                    break;
                case "links":
                    $thumbnail->value = "0";
                    $linkDocument->value = "1";
                    break;
                case "thumbs-links":
                    $thumbnail->value = "1";
                    $linkDocument->value = "1";
                    break;
                case "thumbs":
                    $thumbnail->value = "1";
                    $linkDocument->value = "0";
                    break;
            }
        }

        // Set mode correctly, in case the setting didn't exist yet
        $stylesheetMode = WizardUtils::getXMLField($config, "settings.layout.stylesheet");
        $stylesheetName = WizardUtils::getXMLFieldValue($config, "settings.layout.stylesheetName");
        $stylesheetXML = WizardUtils::getXMLFieldValue($config, "settings.layout.stylesheetXML");
        if ($stylesheetXML && $stylesheetXML != "") {
            $stylesheetMode->value = "xml";
        } elseif ($stylesheetName && $stylesheetName != "") {
            $stylesheetMode->value = "name";
        }

        // Convert bool flags to 0/1
        $bibtexCleaning = WizardUtils::getXMLField($config, "settings.layout.bibtexCleaning");
        $bibtexCleaning->value = filter_var($bibtexCleaning->value, FILTER_VALIDATE_BOOLEAN) ? "1" : "0";
        return $config;
    }

    public function validateForTagCloud(SimpleXMLElement $toValidate): SimpleXMLElement
    {
        // Priorities legacy sheets, so we get rid of duplicate sheets with different names (due to bug in 3.2.X versions)
        $this->handleLegacySheets($toValidate, WizardUtils::TAGCLOUD_LEGACY_SHEETS);

        $config = simplexml_load_file(__DIR__ . "/../../Resources/Private/Update/TagCloudFlexformDefault.xml");
        $this->applyValidatedConfig($toValidate, $config, WizardUtils::TAGCLOUD_FIELDS);

        return $config;
    }

    protected function handleLegacySheets(SimpleXMLElement $xml, array $legacySheets): void
    {
        foreach ($legacySheets as $legacy => $toDelete) {
            $legacySheet = WizardUtils::getFirstElementByXPath($xml, WizardUtils::getXPathForSheet($legacy));
            if ($legacySheet) {
                $toDeleteSheet = WizardUtils::getFirstElementByXPath($xml, WizardUtils::getXPathForSheet($toDelete));
                if ($toDeleteSheet) {
                    unset($toDeleteSheet[0][0]);
                }
            }
        }
    }

    protected function applyValidatedConfig(SimpleXMLElement $toValidate, SimpleXMLElement $config, array $fields): void
    {
        foreach ($fields as $field => $fieldPaths) {
            // Iterate through possible previous value paths
            foreach ($fieldPaths as $fp) {
                $hit = WizardUtils::getXMLFieldValue($toValidate, $fp);
                if ($hit) {
                    $element = WizardUtils::getXMLField($config, $field);
                    $element->value = $hit;
                    break;
                }
            }
        }

        // Validate authentication type
        $authType = WizardUtils::getXMLField($config, "settings.auth.type");
        if ($authType->value == "true" || $authType->value == "beauth") {
            $authType->value = "beauth";
        } else {
            $authType->value = "custom";
        }
    }
}