<?php

namespace AcademicPuma\ExtBibsonomyCsl\Tests\Unit\Update;

use AcademicPuma\ExtBibsonomyCsl\Update\FlexformValidator;
use AcademicPuma\ExtBibsonomyCsl\Utils\WizardUtils;
use SimpleXMLElement;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

class FlexformValidatorTest extends UnitTestCase
{
    const FLEXFORM_PUBLICATION_LIST_PATH = __DIR__ . "/../../resources/flexforms/publication-list/";
    const FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH = __DIR__ . "/../../resources/flexforms/publication-list/valid/";
    const FLEXFORM_TAG_CLOUD_PATH = __DIR__ . "/../../resources/flexforms/tag-cloud/";
    const FLEXFORM_TAG_CLOUD_EXPECTED_PATH = __DIR__ . "/../../resources/flexforms/tag-cloud/valid/";

    public function flexformPublicationListProvider(): array
    {
        return [
            // 0
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_310_db.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_310_db-valid.xml")],
            // 1
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_310_lncs.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_310_lncs-valid.xml")],
            // 2
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_320_everyaware.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_320_everyaware-valid.xml")],
            // 3
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_321_apa.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_321_apa-valid.xml")],
            // 4
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_321_db.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_321_db-valid.xml")],
            // 5
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_322_db.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_322_db-valid.xml")],
            // 6
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_322_from_320_nature.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_322_from_320_nature-valid.xml")],
            // 7
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_322_from_321_everyaware.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_322_from_321_everyaware-valid.xml")],
            // 8
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_322_harvard.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_322_harvard-valid.xml")],
            // 9
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_323_chicago.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_323_chicago-valid.xml")],
            // 10
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_bibsonomy_330_everyaware.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_bibsonomy_330_everyaware-valid.xml")],
            // 11
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_geissler.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_geissler-valid.xml")],
            // 12
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_sek.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_sek-valid.xml")],
            // 13
            [simplexml_load_string(file_get_contents(self::FLEXFORM_PUBLICATION_LIST_PATH . "pl_wachsmuth.xml")),
                file_get_contents(self::FLEXFORM_PUBLICATION_LIST_EXPECTED_PATH . "pl_wachsmuth-valid.xml")],
        ];
    }

    /**
     * @test
     * @dataProvider flexformPublicationListProvider
     * @param SimpleXMLElement $config
     * @param string $expected
     * @return void
     */
    public function textFlexformValidationPublicationList(SimpleXMLElement $config, string $expected): void
    {
        $validator = new FlexformValidator();
        $result = $validator->validateForPublicationList($config);

        // Validate the legacy stylesheet selection
        $legacyStylesheetId = WizardUtils::getXMLFieldValue($config, "settings.bib_stylesheet");
        $result = $validator->validateStylesheet($result, $legacyStylesheetId, []);
        $result = $result->asXML();

        // Assert validated flexform
        $this->assertFlexformEquals($expected, $result);
    }

    public function flexformTagCloudProvider(): array
    {
        return [
            // 0
            [simplexml_load_string(file_get_contents(self::FLEXFORM_TAG_CLOUD_PATH . "tc_bibsonomy_310.xml")),
                file_get_contents(self::FLEXFORM_TAG_CLOUD_EXPECTED_PATH . "tc_bibsonomy_310-valid.xml")],
            // 1
            [simplexml_load_string(file_get_contents(self::FLEXFORM_TAG_CLOUD_PATH . "tc_bibsonomy_320.xml")),
                file_get_contents(self::FLEXFORM_TAG_CLOUD_EXPECTED_PATH . "tc_bibsonomy_320-valid.xml")],
            // 2
            [simplexml_load_string(file_get_contents(self::FLEXFORM_TAG_CLOUD_PATH . "tc_bibsonomy_321.xml")),
                file_get_contents(self::FLEXFORM_TAG_CLOUD_EXPECTED_PATH . "tc_bibsonomy_321-valid.xml")],
            // 3
            [simplexml_load_string(file_get_contents(self::FLEXFORM_TAG_CLOUD_PATH . "tc_bibsonomy_322.xml")),
                file_get_contents(self::FLEXFORM_TAG_CLOUD_EXPECTED_PATH . "tc_bibsonomy_322-valid.xml")],
            // 4
            [simplexml_load_string(file_get_contents(self::FLEXFORM_TAG_CLOUD_PATH . "tc_bibsonomy_323.xml")),
                file_get_contents(self::FLEXFORM_TAG_CLOUD_EXPECTED_PATH . "tc_bibsonomy_323-valid.xml")],
            // 5
            [simplexml_load_string(file_get_contents(self::FLEXFORM_TAG_CLOUD_PATH . "tc_bibsonomy_330.xml")),
                file_get_contents(self::FLEXFORM_TAG_CLOUD_EXPECTED_PATH . "tc_bibsonomy_330-valid.xml")],
        ];
    }

    /**
     * @test
     * @dataProvider flexformTagCloudProvider
     * @param SimpleXMLElement $config
     * @param string $expected
     * @return void
     */
    public function textFlexformValidationTagCloud(SimpleXMLElement $config, string $expected): void
    {
        $validator = new FlexformValidator();
        $result = $validator->validateForTagCloud($config)->asXML();

        // Assert validated flexform
        $this->assertFlexformEquals($expected, $result);
    }

    /**
     * Assert the expected and actual validated flexform configuration as string with all whitespaces removed.
     * Due to the flexform containing nested XML (stylesheets) and CSS, the standard XML assertions from PHPUnit don't work.
     *
     * @param string $expected
     * @param string $actual
     * @return void
     */
    private function assertFlexformEquals(string $expected, string $actual): void
    {
        $expected_no_whitespace = preg_replace('/\s+/', '', $expected);
        $actual_no_whitespace = preg_replace('/\s+/', '', $actual);
        $this->assertEquals($expected_no_whitespace, $actual_no_whitespace);
    }
}